var SHEET_URL = "https://spreadsheets.google.com/feeds/list/1o_1UE8c2SZi71v1YaE4BZf5kd45DGNTHrvq4kH5nLlw/od6/public/values?alt=json"
var INDIA_LATITUDE = 21.0000;
var INDIA_LONGITUDE = 78.0000;
var ZOOM_FACTOR = 5;
var MAX_MARKER_SCALE = 5.0;
var map;

function initialize() {
  var mapOptions = {
    center: new google.maps.LatLng(INDIA_LATITUDE, INDIA_LONGITUDE),
    zoom: ZOOM_FACTOR
  };
  map = new google.maps.Map(document.getElementById("map-canvas"),
  mapOptions);
  fetchData();
}

function fetchData() {
  /* Fetch data as json and assign to 'data'
   logic for populating data should go here
  */
  var rawData;
  var data = [];
  $.get(SHEET_URL, function(sheet) {
    rawData = sheet.feed.entry;
    rawData.forEach(function(row) {
        data.push( {
          name: row.gsx$city.$t,
          events: row.gsx$events.$t,
          participants: row.gsx$participants.$t,
          latitude: row.gsx$latitude.$t,
          longitude: row.gsx$longitude.$t
      });
    });
    populateMap(data);
  });
}

function populateMap(data) {
  var maxParticipants = getMaximumNumberOfParticipants(data);
  // Populate markers with labels containing number of participants
    data.forEach(function(city) {
    var markerScale = MAX_MARKER_SCALE * Number(city.participants)/maxParticipants;
    console.log(markerScale);
    var marker = new MarkerWithLabel({
      position: new google.maps.LatLng(city.latitude, city.longitude),
      draggable: false,
      raiseOnDrag: false,
      map: map,
      icon: {
        path: google.maps.SymbolPath.CIRCLE,
        scale: markerScale,
        fillColor: "red",
        strokeColor: "red",
        fillOpacity: 1.0,
        anchor: new google.maps.Point(0, 1.7)
      },
      labelContent: city.events,
      title: "events: " + city.events,
      labelAnchor: new google.maps.Point(22, 0),
      labelClass: "labels"
    });
  });
}

function getMaximumNumberOfParticipants(data) {
  var maxParticipants = 0.0;
  data.forEach(function(city) {
    var participants = Number(city.participants);
    if (maxParticipants < participants)
      maxParticipants = participants;
  });
  return maxParticipants;
}